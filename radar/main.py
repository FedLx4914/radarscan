#st7789+激光传感器+蜂鸣器+舵机
import math
import utime
import machine
from machine import I2C, Pin, SPI, PWM
import st7789
# 定义I2C总线
i2c = I2C(0, scl = Pin(10), sda = Pin(9), freq = 100000)
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
sensor_address = 0x52 # TOF10120传感器的I2C地址
#蜂鸣器初始化
pwm = PWM(2, Pin(5))
pwm.init(freq=256,duty=0)
#雷达屏幕初始化
R = [20, 40, 60, 80, 100, 120]
ag = [30, 60, 120, 150]
x_start = 120
y_start = 240
radius = 120
angle = 0
interval = 30
direction = -1
display.init()
display.fill(st7789.color565(255,255,255))
LTY = st7789.color565(102,204,255)
b = 0
pin = Pin(12)
pin.init(Pin.OUT)
degree = 0
def iniit():#绘制雷达初始界面
    for i in R:
        display.circle(x_start, y_start, i, st7789.BLUE)
    display.vline(120, 120, 120, st7789.BLUE)
    display.hline(0, 240, 240, st7789.BLUE)
    display.fill_rect(0, 0, 240, 120, st7789.WHITE)
def agle():#绘制雷达辅助角度
    for i in ag:
        display.line(x_start, y_start, 
        x_start + int(radius * math.cos(math.pi * i / 180)), 
        y_start - int(radius * math.sin(math.pi * i / 180)), 
        st7789.BLUE)
def init_tof10120():
    i2c.writeto_mem(sensor_address, 0x00, bytes(0x01))
def read_distance():
    i2c.writeto_mem(sensor_address, 0x00, bytes(0x03))
    utime.sleep_ms(10)
    distance = i2c.readfrom_mem(sensor_address, 0x00, 2)  
    distance_value = (distance[0] << 8) | distance[1]
    return distance_value
iniit()
agle()
init_tof10120()
K = 1 # 确定速度
while True:
    if (degree == 0 or degree == 180):
        degree = 180 - degree #转到0度或180度时掉头
    a = 500 + int(2000 *degree/180) #控制脉冲时间在500ms-2500ms之间
    time1 = 20000 #控制循环内的转动时间
    print("OK")
    for i in range(25): #控制单位时间内高低电平切换的次数
        # 在屏幕上绘制扫描结果
        if(angle <= -180):
            direction = K
        elif(angle >= 0):
            direction = -K
        pin.on()
        #print("i =  angle = ", i, angle)
        while 1:
            if utime.ticks_us() - b >= a:
                b = utime.ticks_us()
                angle += direction
                pin.off()
                break
        #再加一次防止扫描区域出现频闪
        x1 = x_start + int(radius * math.cos(math.pi * angle / 180))
        y1 = y_start + int(radius * math.sin(math.pi * angle / 180))
        x2 = x_start + int(radius * math.cos(math.pi * (angle - direction) / 180))
        y2 = y_start + int(radius * math.sin(math.pi * (angle - direction) / 180))
        display.line(x_start, y_start, x1, y1, LTY)
        display.line(x_start, y_start, x2, y2, st7789.WHITE)
        xi = i * math.cos(math.pi * (angle - direction) / 180)
        yi = i * math.sin(math.pi * (angle - direction) / 180)
        for i in R:
            display.pixel(x_start + int(xi), y_start + int(yi), st7789.BLUE)
        display.vline(120, 120, 120, st7789.BLUE)
        display.hline(0, 240, 240, st7789.BLUE)
        agle()
        display.line(x_start, y_start, x1, y1, LTY)
        distance = read_distance() #读取障碍物距离
#显示文字界面
        display.draw_string(5, 20, 'Distance(cm): %.2f' % (distance / 10), color=st7789.BLUE, bg=st7789.WHITE, size=2, vertical=False, rotate=st7789.ROTATE_0, spacing=1)
        display.draw_string(50, 60, 'WARNING!!!', color=st7789.WHITE, bg=st7789.WHITE, size=2, vertical=False, rotate=st7789.ROTATE_0, spacing=1)
        if distance >= 50: 
            pxdis = (math.sqrt(0.14 * distance)  / 0.14)
            #print("Distance: {} px".format(pxdis))
            x3 = x_start + int(pxdis * math.cos(math.pi * (angle - 1.3 * direction) / 180))
            y3 = y_start + int(pxdis * math.sin(math.pi * (angle - 1.3 * direction) / 180))
            xrp = x3 + int((radius - pxdis) * math.cos(math.pi * (angle - 1.3 * direction) / 180))
            yrp = y3 + int((radius - pxdis) * math.sin(math.pi * (angle - 1.3 * direction) / 180))
            display.line(x_start, y_start, x3, y3, st7789.GREEN)
            display.line(x3, y3, xrp, yrp, st7789.RED)
            while 1:
                if utime.ticks_us() - b >= time1 - a:
                    angle += direction
                    b = utime.ticks_us()
                    break
            angle += direction
        elif 0.1 < distance <= 50:
            print("WARNING!!!")
            display.draw_string(50, 60, 'WARNING!!!', color=st7789.RED, bg=st7789.WHITE, size=2, vertical=False, rotate=st7789.ROTATE_0, spacing=1)   
            while 1:
                if utime.ticks_us() - b >= a:
                    b = utime.ticks_us()
                    angle += direction
                    pin.off()
                    break
            pwm_time = utime.ticks_ms()
            pwm_interval = 250
            while True:
                if utime.ticks_ms() - pwm_time >= pwm_interval:
                    break
                pwm.duty(97)
                pwm.freq(748)
                #弥补显示代码
                if(angle <= -180):
                    direction = 1
                elif(angle >= 0):
                    direction = -1
                #再加一次防止扫描区域出现频闪
                x1 = x_start + int(radius * math.cos(math.pi * angle / 180))
                y1 = y_start + int(radius * math.sin(math.pi * angle / 180))
                x2 = x_start + int(radius * math.cos(math.pi * (angle - direction) / 180))
                y2 = y_start + int(radius * math.sin(math.pi * (angle - direction) / 180))
                display.line(x_start, y_start, x1, y1, LTY)
                display.line(x_start, y_start, x2, y2, st7789.WHITE)
                #校正雷达界面
                for i in R:
               		display.pixel(x_start + int(xi), y_start + int(yi), st7789.BLUE)
                display.vline(120, 120, 120, st7789.BLUE)
                display.hline(0, 240, 240, st7789.BLUE)
                agle()
                display.line(x_start, y_start, x1, y1, LTY)
                distance = read_distance()
                #确定障碍物的距离
                pxdis = (math.sqrt(0.14 * distance)  / 0.14)
                #print("Distance: {} px".format(pxdis))
                x3 = x_start + int(pxdis * math.cos(math.pi * (angle - 1.3 * direction) / 180))
                y3 = y_start + int(pxdis * math.sin(math.pi * (angle - 1.3 * direction) / 180))
                xrp = x3 + int((radius - pxdis) * math.cos(math.pi * (angle - 1.3 * direction) / 180))
                yrp = y3 + int((radius - pxdis) * math.sin(math.pi * (angle - 1.3 * direction) / 180))
                display.line(x_start, y_start, x3, y3, st7789.GREEN)
                display.line(x3, y3, xrp, yrp, st7789.RED)
                angle += direction
            pwm.duty(0)
            while 1:
                if utime.ticks_us() - b >= time1 - a:
                    angle += direction
                    b = utime.ticks_us()
                    break