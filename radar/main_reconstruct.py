import math
import utime
import machine
from machine import I2C, Pin, SPI, PWM
import st7789

# 常量定义
SENSOR_ADDRESS = 0x52  # TOF10120传感器的I2C地址
RADIUS = 120
DISPLAY_WIDTH, DISPLAY_HEIGHT = 240, 240
CENTER_X, CENTER_Y = 120, 240
AGLES = [30, 60, 120, 150]
RINGS = [20, 40, 60, 80, 100, 120]

# 初始化I2C和SPI
i2c = I2C(0, scl=Pin(10), sda=Pin(9), freq=100000)
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
display = st7789.ST7789(spi, DISPLAY_WIDTH, DISPLAY_HEIGHT, reset=Pin(11), dc=Pin(7))

# 蜂鸣器初始化
pwm = PWM(2, Pin(5))
pwm.init(freq=256, duty=0)

# 舵机引脚初始化
pin = Pin(12, Pin.OUT)

# 颜色定义
LTY_COLOR = st7789.color565(102, 204, 255)
BLUE = st7789.BLUE
WHITE = st7789.WHITE
RED = st7789.RED
GREEN = st7789.GREEN

# 绘制雷达初始界面
def draw_radar_interface():
    display.fill(WHITE)
    for r in RINGS:
        display.circle(CENTER_X, CENTER_Y, r, BLUE)
    display.vline(CENTER_X, 120, RADIUS, BLUE)
    display.hline(0, DISPLAY_HEIGHT, DISPLAY_WIDTH, BLUE)
    display.fill_rect(0, 0, DISPLAY_WIDTH, 120, WHITE)

# 绘制雷达辅助角度
def draw_angle_guides():
    for angle in AGLES:
        x = CENTER_X + int(RADIUS * math.cos(math.pi * angle / 180))
        y = CENTER_Y - int(RADIUS * math.sin(math.pi * angle / 180))
        display.line(CENTER_X, CENTER_Y, x, y, BLUE)

# 初始化TOF10120传感器
def init_tof10120():
    i2c.writeto_mem(SENSOR_ADDRESS, 0x00, bytes([0x01]))

# 读取距离
def read_distance():
    i2c.writeto_mem(SENSOR_ADDRESS, 0x00, bytes([0x03]))
    utime.sleep_ms(10)
    distance_data = i2c.readfrom_mem(SENSOR_ADDRESS, 0x00, 2)
    distance_value = (distance_data[0] << 8) | distance_data[1]
    return distance_value

# 显示距离与警告
def display_distance(distance):
    display.draw_string(5, 20, f'Distance(cm): {distance / 10:.2f}', color=BLUE, bg=WHITE, size=2)
    if distance <= 50:
        display.draw_string(50, 60, 'WARNING!!!', color=RED, bg=WHITE, size=2)

# 控制舵机角度
def control_servo(angle, direction, step=1):
    if angle <= -180:
        direction = step
    elif angle >= 0:
        direction = -step
    return angle + direction

# 蜂鸣器报警
def trigger_buzzer(duration_ms=250, freq=748):
    pwm.duty(97)
    pwm.freq(freq)
    utime.sleep_ms(duration_ms)
    pwm.duty(0)

# 主程序
def main():
    angle = 0
    direction = -1
    b = utime.ticks_us()
    time1 = 20000

    draw_radar_interface()
    draw_angle_guides()
    init_tof10120()

    while True:
        distance = read_distance()

        # 控制舵机和蜂鸣器
        angle = control_servo(angle, direction)
        display_distance(distance)

        # 扫描区域显示
        x1 = CENTER_X + int(RADIUS * math.cos(math.pi * angle / 180))
        y1 = CENTER_Y + int(RADIUS * math.sin(math.pi * angle / 180))
        display.line(CENTER_X, CENTER_Y, x1, y1, LTY_COLOR)

        # 障碍物检测
        if distance <= 50:
            trigger_buzzer()
        else:
            utime.sleep_ms(100)  # 正常延时

        # 防止频闪，绘制界面
        draw_radar_interface()
        draw_angle_guides()

# 启动主程序
if __name__ == "__main__":
    main()
